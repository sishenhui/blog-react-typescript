-- `使用的扩展包`
📂 System
├── 📂 Plugin
│ ├── 📄 file-loader | `在处理图片文件时，Webpack 会将它们复制到输出目录并生成一个哈希值文件名`
│ ├── 📄 style-loader && css-loader | `Webpack 将会使用 css-loader 解析 CSS 文件，并使用 style-loader 将其转化为可应用的样式`
│ ├── 📄 <NULL>
│ └── 📄 <NULL>
└────────────────────
