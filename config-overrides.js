const {
  override,
  addWebpackAlias,
  addWebpackModuleRule,
} = require("customize-cra");
const path = require("path");

module.exports = override(
  // @别名
  addWebpackAlias({
    "@": path.resolve("./src"),
  }),
  // scss全局变量
  addWebpackModuleRule({
    test: /\.scss$/,
    use: [
      "style-loader",
      "css-loader",
      "sass-loader",
      {
        loader: "sass-resources-loader",
        options: {
          resources: ["./src/assets/scss/varable.scss"],
        },
      },
    ],
  })
);
