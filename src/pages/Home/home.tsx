import React from "react";
import Layout from "../../../src/components/Layout";

const Home: React.FC = () => {
  return (
    <Layout>
      <div className="">home</div>
    </Layout>
  );
};

export default Home;
