import React, { ReactNode } from "react";

interface LayoutProps {
  children: ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const layoutStyle: React.CSSProperties = {
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
  };

  const mainStyle: React.CSSProperties = {
    backgroundColor: `rgb(248,248,248)`,
    flex: 1,
  };

  return (
    <div style={layoutStyle}>
      <header>header</header>
      <main style={mainStyle}>{children}</main>
      <footer>footer</footer>
    </div>
  );
};

export default Layout;
