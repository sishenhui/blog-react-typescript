import React from "react";
import Router from "../src/router/index";
import "../src/styles/layout.css";

const App: React.FC = () => {
  return (
    <div className="App">
      <Router />
    </div>
  );
};

export default App;
