import React, { Fragment, Component } from "react";
import Home from "../pages/Home/home";
import Login from "../pages/Login/login";
import { BrowserRouter, Route, Routes } from "react-router-dom";

export default class index extends Component {
  render() {
    return (
      <Fragment>
        {/* 所有的路由配置均在 BrowserRouter 内部 */}
        <BrowserRouter>
          {/* react V6 Switch改为Routers */}
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
          </Routes>
        </BrowserRouter>
      </Fragment>
    );
  }
}
