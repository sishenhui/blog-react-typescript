const path = require("path");

module.exports = {
  mode: "development", // 指定环境为开发环境
  entry: "./src/index.tsx", // 指定入口文件路径
  output: {
    path: path.resolve(__dirname, "dist"), // 指定输出文件路径
    filename: "bundle.js", // 指定输出文件名
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/, // 匹配 ts 或 tsx 文件
        exclude: /node_modules/, // 排除 node_modules 文件夹
        use: {
          loader: "babel-loader", // 使用 babel-loader 处理匹配到的文件
          options: {
            presets: ["@babel/preset-react"], // 使用 @babel/preset-react 插件转译 React 代码
          },
        },
      },
      {
        test: /\.css$/, // 匹配 css 文件
        use: ["style-loader", "css-loader"], // 使用 style-loader 和 css-loader 处理匹配到的文件
      },
      {
        test: /\.(jpg|jpeg|png|gif)$/i, // 匹配 jpg、jpeg、png 或 gif 文件
        type: "asset/resource", // 使用 asset 模块类型处理匹配到的文件，生成资源文件
      },
    ],
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx"], // 自动解析指定的扩展名文件
  },
};
